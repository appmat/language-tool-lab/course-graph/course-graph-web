package ru.appmat
import kotlinx.serialization.Serializable

@Serializable
data class Subject(
        val id:Long,
        val name:String,
        val description:String
) {

}