package ru.appmat

import io.ktor.locations.*

@Location("/graphcourse")
class Graph{
    @Location("/home")
    class Home(val api:Graph)

    @Location("/courses")
    data class Courses(val parent: Graph)

    @Location("/specialties")
    data class Specialties(val parent: Graph){
        @Location("/{id}")
        data class CoursesId(val id:Long, val parent: Specialties)
    }

    @Location("/subjects")
    data class Subjects(val parent: Graph)
}